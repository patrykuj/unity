﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class NetworkManager : MonoBehaviour {

	public GameObject standbyCamera;
	SpawnSpot[] spawnSpots;

	void Start () {
		spawnSpots = GameObject.FindObjectsOfType<SpawnSpot> ();
		Connect ();
	}

	void Connect(){
		PhotonNetwork.ConnectUsingSettings ("mFPS v1.0 ");
	}

	void OnGUI(){
		GUILayout.Label (PhotonNetwork.connectionStateDetailed.ToString());
	}

	void OnJoinedLobby() {
		PhotonNetwork.JoinRandomRoom ();
	}

	void OnPhotonRandomJoinFailed(){
		PhotonNetwork.CreateRoom (null);
	}

	void OnJoinedRoom(){
		SpawnMyPlayer ();
	}

	void SpawnMyPlayer(){
		if (spawnSpots == null) {
			Debug.LogError ("Spawn spot is null!");
			return;
		}
		SpawnSpot spot = spawnSpots[Random.Range(0, spawnSpots.Length)];
		GameObject playerGameObject = (GameObject) PhotonNetwork.Instantiate ("PlayerController", spot.transform.position, spot.transform.rotation, 0);
		standbyCamera.SetActive(false);
		playerGameObject.GetComponent<FirstPersonController> ().enabled = true;
		playerGameObject.GetComponentInChildren<Camera> ().enabled = true;
	}
}
